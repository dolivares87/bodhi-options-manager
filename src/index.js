import 'source-map-support/register';

export default function settingsManager(options = {}) {
  const { settings = {}, state = {} } = options;

  return {
    getSetting(setting) {
      return settings[setting];
    },
    getState(stateProperty) {
      return state[stateProperty];
    }
  };
}
