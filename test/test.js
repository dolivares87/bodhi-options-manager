import test from 'ava';
import optionsManager from '../src';

test('Parse options passed in', t => {
  const fixture = {
    settings: {
      settingOne: 'hi',
      settingTwo: 1
    },
    state: {
      stateOne: 'state one test',
      stateTwo: 123
    }
  };

  const optsHandler = optionsManager(fixture);

  t.is(optsHandler.getSetting('settingTwo'), 1, 'Should match settingTwo through API call');
  t.is(optsHandler.getSetting('settingOne'), 'hi', 'Should match settingOne through API call');
  t.is(optsHandler.getState('stateOne'), 'state one test',
    'Should match stateOne through API call');
  t.is(optsHandler.getState('stateTwo'), 123, 'Should match stateTwo through API call');
});
